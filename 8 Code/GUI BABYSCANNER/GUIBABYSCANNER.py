import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow

from UI_GUI import *
class var:
    LASERWaarde=0.0
    LASERSTATE = False

class MainWindow:
    def __init__(self):
       
        self.main_win = QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.main_win)

        self.ui.stackedWidget.setCurrentWidget(self.ui.HOMESCREEN)

        self.ui.Meting_btn.clicked.connect(self.MetingScreen)
        self.ui.Opgeslagen_btn.clicked.connect(self.Opgeslagencreen)
        self.ui.Laser_btn.clicked.connect(self.LaserScreen)

        self.ui.BACKMETING_btn.clicked.connect(self.backHome)
        # self.ui.BACKOPGESLAGEN_btn.clicked.connect(self.backHome)
        
        # LASERMENU###############################################################################################################################
        self.ui.LASERVALUE_PLUS1.clicked.connect(lambda: self.LASERVALUESEG(1))
        self.ui.LASERVALUE_PLUS01.clicked.connect(lambda: self.LASERVALUESEG(0.1))
        self.ui.LASERVALUE_MIN1.clicked.connect(lambda: self.LASERVALUESEG(-1))
        self.ui.LASERVALUE_MIN01.clicked.connect(lambda: self.LASERVALUESEG(-0.1))
        self.ui.LASERAANUIT_btn.clicked.connect(self.LaserAanUint)
        self.ui.BACKLASER_btn.clicked.connect(self.backHome)
    
    def show(self):
        self.main_win.show()
    def MetingScreen(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.METINGSCREEN)
    def Opgeslagencreen(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.OPGESLAGENSCREEN)
    def LaserScreen(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.LASERSCREEN)
    def LaserAanUint(self):
        if var.LASERSTATE == False: 
            self.ui.LASERAANUIT_btn.setText("Laser UIT")
            var.LASERSTATE = True
        elif var.LASERSTATE == True: 
            var.LASERSTATE = False
            self.ui.LASERAANUIT_btn.setText("Laser AAN")
            var.LASERSTATE = False
        print(var.LASERSTATE)
    def LASERVALUESEG(self,LASER_VALUE):
        var.LASERWaarde =round((LASER_VALUE+var.LASERWaarde),1)
        if var.LASERWaarde > 10 :var.LASERWaarde = 10
        elif var.LASERWaarde < 0 :var.LASERWaarde = 0
        digit1 =int(var.LASERWaarde/10)
        digit2 =int(var.LASERWaarde%10)
        digit3= int(var.LASERWaarde*10)%10

        self.ui.LASERVALUE10.display(digit1)
        self.ui.LASERVALUE.display(digit2)
        self.ui.LASERVALUEDIGIT.display(digit3)





    def backHome(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.HOMESCREEN)



if __name__ =='__main__':
    app= QApplication(sys.argv)
    main_win =MainWindow()
    main_win.show()
    sys.exit(app.exec_())


