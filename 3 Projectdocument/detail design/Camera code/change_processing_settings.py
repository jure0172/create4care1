import cv2
import json
import numpy as np
from numba import njit


def emptyfunc(a):
    pass


@njit
def remove_baseline(baseline_image, measurement_image, height, width):
    resultimage = baseline_image
    resultimage = resultimage*0
    for i in range(height):
        for j in range(width):
            # controlling measuremnt_image first saves 5s per picture.
            if((measurement_image[i, j] == 255 and baseline_image[i, j] == 0)):
                resultimage[i, j] = 255
    return resultimage


# make sure to select the right baseline and baby image!
BaseImg = cv2.imread("reference_images/Referentie__80__2592x1944_26_6_2019.jpg")
BabyImg = cv2.imread("reference_images/Top_9__90__2592x1944_26_6_2019.jpg")

ImageHeight, ImageWidth, dimensions = BaseImg.shape
BaseImg = cv2.cvtColor(BaseImg, cv2.COLOR_BGR2GRAY)
OriginalCorners = np.float32([[0, 0],
                              [0, ImageHeight],
                              [ImageWidth, ImageHeight],
                              [ImageWidth, 0]])
WarpCorners = np.float32([[609, 329], [745, 1295],
                          [1915, 1307], [2075, 323]])
WarpMatrix = cv2.getPerspectiveTransform(WarpCorners, OriginalCorners)
BaseImg = cv2.warpPerspective(BaseImg, WarpMatrix, (ImageWidth,
                                                    ImageHeight))

BabyImg = cv2.cvtColor(BabyImg, cv2.COLOR_BGR2GRAY)
WarpMatrix = cv2.getPerspectiveTransform(WarpCorners, OriginalCorners)
BabyImg = cv2.warpPerspective(BabyImg, WarpMatrix, (ImageWidth,
                                                    ImageHeight))


with open('settings.json', 'r') as f:
    settings = json.load(f)
BaseThreshold = settings['BaseThreshold']
BaseDilValue = settings['BaseDilValue']
BaseEroValue = settings['BaseEroValue']
BabyThreshold = settings['BabyThreshold']
BabyDilValue = settings['BabyDilValue']
BabyEroValue = settings['BabyEroValue']
ResultDilValue = settings['ResultDilValue']
ResultEroValue = settings['ResultEroValue']
cv2.namedWindow("Trackbar")
cv2.resizeWindow("Trackbar", 1500, 500)
cv2.createTrackbar("BaseThreshold:", "Trackbar", BaseThreshold, 255, emptyfunc)
cv2.createTrackbar("BaseDilValue:", "Trackbar", BaseDilValue, 30, emptyfunc)
cv2.createTrackbar("BaseEroValue:", "Trackbar", BaseEroValue, 30, emptyfunc)
cv2.createTrackbar("BabyThreshold:", "Trackbar", BabyThreshold, 255, emptyfunc)
cv2.createTrackbar("BabyDilValue:", "Trackbar", BabyDilValue, 30, emptyfunc)
cv2.createTrackbar("BabyEroValue:", "Trackbar", BabyEroValue, 30, emptyfunc)
cv2.createTrackbar("ResultDilValue:", "Trackbar", ResultDilValue, 30, emptyfunc)
cv2.createTrackbar("ResultEroValue:", "Trackbar", ResultEroValue, 30, emptyfunc)
while True:
    # read trackbars
    BaseThreshold = cv2.getTrackbarPos("BaseThreshold:", "Trackbar")
    BaseDilValue = cv2.getTrackbarPos("BaseDilValue:", "Trackbar")
    BaseEroValue = cv2.getTrackbarPos("BaseEroValue:", "Trackbar")
    BabyThreshold = cv2.getTrackbarPos("BabyThreshold:", "Trackbar")
    BabyDilValue = cv2.getTrackbarPos("BabyDilValue:", "Trackbar")
    BabyEroValue = cv2.getTrackbarPos("BabyEroValue:", "Trackbar")
    ResultDilValue = cv2.getTrackbarPos("ResultDilValue:", "Trackbar")
    ResultEroValue = cv2.getTrackbarPos("ResultEroValue:", "Trackbar")

    # make b/w
    (Thresh, BwBaseImg) = cv2.threshold(BaseImg, BaseThreshold, 255,
                                        cv2.THRESH_BINARY)
    (Thresh, BwBabyImg) = cv2.threshold(BabyImg, BabyThreshold, 255,
                                        cv2.THRESH_BINARY)

    BaseDilKernel = np.ones((BaseDilValue, BaseDilValue), np.uint8)
    BabyDilKernel = np.ones((BabyDilValue, BabyDilValue), np.uint8)
    ResultDilKernel = np.ones((ResultDilValue, ResultDilValue), np.uint8)
    BaseEroKernel = np.ones((BaseEroValue, BaseEroValue), np.uint8)
    BabyEroKernel = np.ones((BabyEroValue, BabyEroValue), np.uint8)
    ResultEroKernel = np.ones((ResultEroValue, ResultEroValue), np.uint8)
    # dilute/erode
    DilBaseImg = cv2.dilate(BwBaseImg, BaseDilKernel, iterations=1)
    DilBabyImg = cv2.dilate(BwBabyImg, BabyDilKernel, iterations=1)
    EroBaseImg = cv2.erode(DilBaseImg, BaseEroKernel, iterations=1)
    EroBabyImg = cv2.erode(DilBabyImg, BabyEroKernel, iterations=1)

    ResultImage = remove_baseline(EroBaseImg, EroBabyImg, ImageHeight, ImageWidth)
    ResultImage = np.uint8(ResultImage)  # without this we can't use cv2 functions.
    ResultImage = cv2.dilate(ResultImage, ResultDilKernel, iterations=1)
    ResultImage = cv2.erode(ResultImage, ResultEroKernel, iterations=1)

    EroBaseImgResized = cv2.resize(EroBaseImg,
                                   (int(ImageWidth*0.4), int(ImageHeight*0.4)))
    EroBabyImgResized = cv2.resize(EroBabyImg,
                                   (int(ImageWidth*0.4), int(ImageHeight*0.4)))
    ResultImageResized = cv2.resize(ResultImage,
                                    (int(ImageWidth*0.4), int(ImageHeight*0.4)))
    cv2.imshow("baseline", EroBaseImgResized)
    cv2.imshow("baby", EroBabyImgResized)
    cv2.imshow("result", ResultImageResized)
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()

settings['BaseThreshold'] = BaseThreshold
settings['BaseDilValue'] = BaseDilValue
settings['BaseEroValue'] = BaseEroValue
settings['BabyThreshold'] = BabyThreshold
settings['BabyDilValue'] = BabyDilValue
settings['BabyEroValue'] = BabyEroValue
settings['ResultDilValue'] = ResultDilValue
settings['ResultEroValue'] = ResultEroValue
# dump library in a json file
with open('settings.json', 'w') as f:
    json.dump(settings, f)

cv2.imwrite("processing_settings.jpg", ResultImage)
