import cv2
import json
import numpy as np


def emptyfunc(a):
    pass


# make sure to select the right image!
img = cv2.imread("reference_images/Referentie__80__2592x1944_26_6_2019.jpg")
ImageHeight, ImageWidth, dimensions = img.shape
img = cv2.resize(img, (int(ImageWidth*0.4), int(ImageHeight*0.4)))

with open('settings.json', 'r') as f:
    settings = json.load(f)
HueMin = settings['HueMin']
HueMax = settings['HueMax']
SaturationMin = settings['SaturationMin']
SaturationMax = settings['SaturationMax']
ValueMin = settings['ValueMin']
ValueMax = settings['ValueMax']

cv2.namedWindow("Trackbars")
cv2.resizeWindow("Trackbars", 600, 350)
cv2.createTrackbar("Hue min:", "Trackbars", HueMin, 179, emptyfunc)
cv2.createTrackbar("Hue max:", "Trackbars", HueMax, 179, emptyfunc)
cv2.createTrackbar("Sat min:", "Trackbars", SaturationMin, 255, emptyfunc)
cv2.createTrackbar("Sat max:", "Trackbars", SaturationMax, 255, emptyfunc)
cv2.createTrackbar("Val min:", "Trackbars", ValueMin, 255, emptyfunc)
cv2.createTrackbar("Val max:", "Trackbars", ValueMax, 255, emptyfunc)

ImgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
while True:
    HueMin = cv2.getTrackbarPos("Hue min:", "Trackbars")
    HueMax = cv2.getTrackbarPos("Hue max:", "Trackbars")
    SaturationMin = cv2.getTrackbarPos("Sat min:", "Trackbars")
    SaturationMax = cv2.getTrackbarPos("Sat max:", "Trackbars")
    ValueMin = cv2.getTrackbarPos("Val min:", "Trackbars")
    ValueMax = cv2.getTrackbarPos("Val max:", "Trackbars")
    LowerArray = np.array([HueMin, SaturationMin, ValueMin])
    UpperArray = np.array([HueMax, SaturationMax, ValueMax])

    mask = cv2.inRange(ImgHSV, LowerArray, UpperArray)
    ImgResult = cv2.bitwise_and(img, img, mask=mask)

    cv2.imshow("Original Image", img)
    cv2.imshow("masked image", ImgResult)
    cv2.waitKey(1)
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()

settings['HueMin'] = HueMin
settings['HueMax'] = HueMax
settings['SaturationMin'] = SaturationMin
settings['SaturationMax'] = SaturationMax
settings['ValueMin'] = ValueMin
settings['ValueMax'] = ValueMax
with open('settings.json', 'w') as f:
    json.dump(settings, f)
