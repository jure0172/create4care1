import sys
import os
import time
import cv2
import json
import numpy as np
import csv
from datetime import datetime
import UART
from math import tan
import matplotlib.tri as triang
from stl import mesh
from numba import njit


IMAGE_WIDTH_MM = 200
DISTANCE_BETWEEN_LINES_MM = 50
LINES_PER_IMAGE = 13  # gets determined automatically.
DISTANCE_TO_CAMERA = 30
AMOUNT_OF_IMAGES = 2


# used to print colored text in terminal
class Colors:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[36m'
    END = '\033[m'


def emptyfunc(a):
    pass


@njit
def remove_baseline(baseline_image, measurement_image, height, width):
    resultimage = baseline_image
    resultimage = resultimage*0
    for i in range(height):
        for j in range(width):
            # controlling measuremnt_image first saves 5s per picture.
            if((measurement_image[i, j] == 255 and baseline_image[i, j] == 0)):
                resultimage[i, j] = 255
    return resultimage


@njit
def calculate_pixheight(height, width, array, resultarray,
                        baseline_array, amountofimages, currentimage):
    for i in range(height):
        if(i <= baseline_array[0]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 0*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[0] - i
        elif(i <= baseline_array[1]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 1*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[1] - i
        elif(i <= baseline_array[2]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 2*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[2] - i
        elif(i <= baseline_array[3]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 3*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[3] - i
        elif(i <= baseline_array[4]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 4*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[4] - i
        elif(i <= baseline_array[5]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 5*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[5] - i
        elif(i <= baseline_array[6]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 6*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[6] - i
        elif(i <= baseline_array[7]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 7*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[7] - i
        elif(i <= baseline_array[8]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 8*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[8] - i
        elif(i <= baseline_array[9]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 9*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[9] - i
        elif(i <= baseline_array[10]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 10*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[10] - i
        elif(i <= baseline_array[11]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 11*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[11] - i
        elif(i <= baseline_array[12]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 12*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[12] - i
        elif(i <= baseline_array[13]):
            for j in range(width):
                if(array[i, j] == 255):
                    temp1 = 13*amountofimages
                    temp2 = ((amountofimages-1)-currentimage)
                    resultarray[(temp1 + temp2), j] = baseline_array[13] - i


@njit
def convert_height(array, resultarray, amountofimages,
                   linesperimage, width, cameradistance):
    for i in range(amountofimages * linesperimage):
        for j in range(width):
            px = array[i, j]  # px is objecthoogte in pixels zoals gemeten.
            if px != 0:
                resultarray[i, j] = (cameradistance) / (tan(0.0014*px+0.2686))
            else:
                resultarray[i, j] = 0


@njit
def get_object_width(objectimage, imagewidth, amountofimages, linesperimage):
    arrayheight = amountofimages * linesperimage
    matchfound = 0
    for j in range(imagewidth):
        for i in range(arrayheight):
            if(objectimage[i, j] != 0) & (matchfound == 0):
                width_cursor_1 = j
                matchfound = 1
    matchfound = 0
    j = imagewidth-1  # -1 because index starts at 0
    while(j > 0):
        i = arrayheight-1
        while(i > 0):
            if(objectimage[i, j] != 0) & (matchfound == 0):
                width_cursor_2 = j
                matchfound = 1
            i -= 1
        j -= 1
    return width_cursor_2 - width_cursor_1


@njit
def get_object_length(objectimage, imagewidth, amountofimages, linesperimage):
    arrayheight = amountofimages * linesperimage
    matchfound = 0
    for i in range(arrayheight):
        for j in range(imagewidth):
            if(objectimage[i, j] != 0) & (matchfound == 0):
                height_cursor_1 = i
                matchfound = 1
    matchfound = 0
    i = arrayheight-1  # -1 because index starts at 0
    while(i > 0):
        j = imagewidth-1
        while(j > 0):
            if(objectimage[i, j] != 0) & (matchfound == 0):
                height_cursor_2 = i
                matchfound = 1
            j -= 1
        i -= 1
    return height_cursor_2 - height_cursor_1


def filter_colors(image):
    with open('settings.json', 'r') as f:
        settings = json.load(f)
    HueMin = settings['HueMin']
    HueMax = settings['HueMax']
    SaturationMin = settings['SaturationMin']
    SaturationMax = settings['SaturationMax']
    ValueMin = settings['ValueMin']
    ValueMax = settings['ValueMax']

    ImgHSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    LowerArray = np.array([HueMin, SaturationMin, ValueMin])
    UpperArray = np.array([HueMax, SaturationMax, ValueMax])
    mask = cv2.inRange(ImgHSV, LowerArray, UpperArray)

    return cv2.bitwise_and(image, image, mask=mask)


def process_images(leftorright):
    # used to init the result array just once.
    result_init = 0
    for CURRENT_IMAGE in range(AMOUNT_OF_IMAGES):
        # type ", 0" behind filename to read in black/white.
        BaseImg = cv2.imread("ImagesTaken/baseline_"+leftorright+".jpg")
        BabyImg = cv2.imread("ImagesTaken/img_"+str(CURRENT_IMAGE)+"_"+leftorright+".jpg")
        if(BaseImg is None) or (BabyImg is None):
            print(Colors.RED, "\r\n image load failed \n", Colors.END)
            sys.exit()
        # -------------------------FILTER COLORS--------------------------------
        BaseImg = filter_colors(BaseImg)
        BabyImg = filter_colors(BabyImg)
        # Get height and width
        ImageHeight, ImageWidth, dimensions = BaseImg.shape
        # make the image gray
        BaseImg = cv2.cvtColor(BaseImg, cv2.COLOR_BGR2GRAY)
        BabyImg = cv2.cvtColor(BabyImg, cv2.COLOR_BGR2GRAY)
        # warp the images to get the right perspective.
        # OriginalCorners contains the original corners of the image.
        # WarpCorners contains the new corners.
        # sequence: top left, bottom left, bottom right, top right.
        OriginalCorners = np.float32([[0, 0],
                                     [0, ImageHeight],
                                     [ImageWidth, ImageHeight],
                                     [ImageWidth, 0]])
        WarpCorners = np.float32([[609, 329], [745, 1295],
                                 [1915, 1307], [2075, 323]])
        # link original corners to warp corners
        WarpMatrix = cv2.getPerspectiveTransform(WarpCorners, OriginalCorners)
        # Use warp matrix to warp the images
        BaseImg = cv2.warpPerspective(BaseImg, WarpMatrix, (ImageWidth,
                                                            ImageHeight))
        BabyImg = cv2.warpPerspective(BabyImg, WarpMatrix, (ImageWidth,
                                                            ImageHeight))
        # Get settings from json file and set the variables.
        with open('settings.json', 'r') as f:
            settings = json.load(f)
        BaseThreshold = settings['BaseThreshold']
        BaseDilValue = settings['BaseDilValue']
        BaseEroValue = settings['BaseEroValue']
        BabyThreshold = settings['BabyThreshold']
        BabyDilValue = settings['BabyDilValue']
        BabyEroValue = settings['BabyEroValue']
        ResultDilValue = settings['ResultDilValue']
        ResultEroValue = settings['ResultEroValue']
        # make b/w
        (Thresh, BwBaseImg) = cv2.threshold(BaseImg, BaseThreshold, 255,
                                            cv2.THRESH_BINARY)
        (Thresh, BwBabyImg) = cv2.threshold(BabyImg, BabyThreshold, 255,
                                            cv2.THRESH_BINARY)
        # make dilution/erosion kernels
        BaseDilKernel = np.ones((BaseDilValue, BaseDilValue), np.uint8)
        BabyDilKernel = np.ones((BabyDilValue, BabyDilValue), np.uint8)
        BaseEroKernel = np.ones((BaseEroValue, BaseEroValue), np.uint8)
        BabyEroKernel = np.ones((BabyEroValue, BabyEroValue), np.uint8)
        # dilute/erode the images
        DilBaseImg = cv2.dilate(BwBaseImg, BaseDilKernel, iterations=1)
        DilBabyImg = cv2.dilate(BwBabyImg, BabyDilKernel, iterations=1)
        EroBaseImg = cv2.erode(DilBaseImg, BaseEroKernel, iterations=1)
        EroBabyImg = cv2.erode(DilBabyImg, BabyEroKernel, iterations=1)

        # -----------------------------FINDING BASELINES---------------------------
        baselines = []
        i = 0
        while(i < ImageHeight):
            if (EroBaseImg[i, int(ImageWidth/2)] == 0):
                i += 1
            # start of line found
            elif (EroBaseImg[i, int(ImageWidth/2)] == 255):
                PixelCounter = 0
                # count line thickness
                while(EroBaseImg[i, int(ImageWidth/2)] == 255):
                    PixelCounter = PixelCounter + 1
                    i += 1
                # at end of the line, add half the thickness to the current height
                # to find the center of the baseline
                baselines.append(int(i - PixelCounter/2))
        i = 0
        LINES_PER_IMAGE = len(baselines)  # used for calculating distances
        baselines_numpy = np.array(baselines)  # this can be used in NJIT functions.
        # -----------------------REMOVE BASELINE FROM MEASUREMENT------------------
        ResultImage = remove_baseline(EroBaseImg, EroBabyImg, ImageHeight,
                                      ImageWidth)
        ResultDilKernel = np.ones((ResultDilValue, ResultDilValue), np.uint8)
        ResultEroKernel = np.ones((ResultEroValue, ResultEroValue), np.uint8)
        ResultImage = cv2.dilate(ResultImage, ResultDilKernel, iterations=1)
        ResultImage = cv2.erode(ResultImage, ResultEroKernel, iterations=1)
        # --------------DETECT HEIGHT BETWEEN BASELINE AND MEASUREMENT-------------
        # this code now overwrites measurements from the pixels above,
        # meaning the underside of the displaced laser line is measured.
        # it might be more accurate to calculate the middle.
        if(result_init == 0):
            result_init = 1
            result = np.zeros((AMOUNT_OF_IMAGES * LINES_PER_IMAGE, ImageWidth),
                              np.int)
        calculate_pixheight(ImageHeight, ImageWidth, ResultImage, result,
                            baselines_numpy, AMOUNT_OF_IMAGES, CURRENT_IMAGE)

    # ------------------CONVERT HEIGHT------------------------
    hoogte = np.zeros((AMOUNT_OF_IMAGES * LINES_PER_IMAGE, ImageWidth), np.int)
    convert_height(result, hoogte, AMOUNT_OF_IMAGES,
                   LINES_PER_IMAGE, ImageWidth, DISTANCE_TO_CAMERA)

    # ----------------GET HEIGHT AND WIDTH---------------------
    MeasuredWidth = get_object_width(hoogte, ImageWidth, AMOUNT_OF_IMAGES, LINES_PER_IMAGE)

    MeasuredLength = get_object_length(hoogte, ImageHeight, AMOUNT_OF_IMAGES, LINES_PER_IMAGE)

    pixels_per_mm = ImageWidth / IMAGE_WIDTH_MM  # this will also be used for making the CSV file.
    MeasuredWidth = MeasuredWidth / pixels_per_mm
    MeasuredLength = (DISTANCE_BETWEEN_LINES_MM / AMOUNT_OF_IMAGES) * MeasuredLength
    MeasuredWidth = round(MeasuredWidth, 2)
    MeasuredLength = round(MeasuredLength, 2)
    JSONresult = {}
    JSONresult['width'] = MeasuredWidth
    JSONresult['length'] = MeasuredLength
    with open(DateTimeString+'/'+DateTimeString+'_'+leftorright+'.json', 'w') as f:
        json.dump(JSONresult, f)
    # ----------------MAKE CSV FILE ---------------------------
    DataList = []
    # convert X and Y values to mm. Z is already converted, we copy this value.
    for i in range(AMOUNT_OF_IMAGES * LINES_PER_IMAGE):
        for j in range(ImageWidth):
            XValue = i*(DISTANCE_BETWEEN_LINES_MM / AMOUNT_OF_IMAGES)
            YValue = 1 / (pixels_per_mm / (j + 1))
            ZValue = hoogte[i, j]
            DataList.append([XValue, YValue, ZValue])
    with open(DateTimeString+'/'+DateTimeString+'_'+leftorright+'.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerows(DataList)
    # ----------------------GENERATE STL FILE------------------------
    xyz = np.genfromtxt(DateTimeString+'/'+DateTimeString+'_'+leftorright+'.csv', delimiter=',')
    xyz = np.nan_to_num(xyz)

    x = xyz[:, 0]
    y = xyz[:, 1]
    z = xyz[:, 2]

    tri = triang.Triangulation(x, y)
    data = np.zeros(len(tri.triangles), dtype=mesh.Mesh.dtype)
    BabyMesh = mesh.Mesh(data, remove_empty_areas=False)
    BabyMesh.x[:] = x[tri.triangles]
    BabyMesh.y[:] = y[tri.triangles]
    BabyMesh.z[:] = z[tri.triangles]
    BabyMesh.save(DateTimeString + '/' + DateTimeString + '_'+leftorright + '.stl')


def read_images(images):
    cam1 = cv2.VideoCapture(0)
    cam2 = cv2.VideoCapture(1)
    if cam1 is None or not cam1.isOpened():
        print(Colors.RED, "cam 1 not opened.", Colors.END)
        # sys.exit()
    elif cam2 is None or not cam2.isOpened():
        print(Colors.RED, "cam 2 not opened.", Colors.END)
        # sys.exit()

    with open('settings.json', 'r') as f:
        settings = json.load(f)
    CamBacklight = settings['CamBacklight']
    CamBrightness = settings['CamBrightness']
    CamContrast = settings['CamContrast']
    CamGain = settings['CamGain']

    cam1.set(cv2.CAP_PROP_BACKLIGHT, CamBacklight)
    cam1.set(cv2.CAP_PROP_BRIGHTNESS, CamBrightness)
    cam1.set(cv2.CAP_PROP_CONTRAST, CamContrast)
    cam1.set(cv2.CAP_PROP_GAIN, CamGain)

    cam2.set(cv2.CAP_PROP_BACKLIGHT, CamBacklight)
    cam2.set(cv2.CAP_PROP_BRIGHTNESS, CamBrightness)
    cam2.set(cv2.CAP_PROP_CONTRAST, CamContrast)
    cam2.set(cv2.CAP_PROP_GAIN, CamGain)

    # capture one image so the camera activates
    succes, img1 = cam1.read()
    succes, img2 = cam2.read()
    # wait for camera to show normal image
    time.sleep(15)
    for i in range(images):
        # right laser on, wait for confirmation, read image, right laser off
        # UART.UartSend("B001")
        time.sleep(0.5)
        succes, img1 = cam1.read()
        # UART.UartSend("B000")
        # cv2.imwrite(DateTimeString+'/'+"img_"+str(i)+"_right.jpg", img1)
        # left laser on, wait for confirmation, read image, left laser off
        # UART.UartSend("B010")
        time.sleep(0.5)
        succes, img2 = cam2.read()
        # UART.UartSend("B000")
        # cv2.imwrite(DateTimeString+'/'+"img_"+str(i)+"_left.jpg", img2)
        # tell motors to move
        # UART.UartSend("A00"+str(i))
        time.sleep(0.5)


def start_meting():
    # get current date and time and create a directory with them.
    # the datetimestring is saved as global so other functions can store
    # their results with that name.
    now = datetime.now()
    global DateTimeString
    DateTimeString = now.strftime("%d_%m_%Y__%H_%M_%S")
    print("date and time =", DateTimeString)
    os.mkdir(DateTimeString)
    read_images(AMOUNT_OF_IMAGES)
    # the name given to the function is the name given in read_images.
    process_images('left')
    process_images('right')


# ------------------TEST---------------
start_meting()
