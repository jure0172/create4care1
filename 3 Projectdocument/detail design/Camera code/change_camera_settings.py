import cv2
import json


def emptyfunc(a):
    pass


class Colors:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[36m'
    END = '\033[m'


# open camera
cap = cv2.VideoCapture(0)
# check if camera is opened
if cap is None or not cap.isOpened():
    print(Colors.RED, "camera not opened.", Colors.END)
    while 1:
        continue
# get settings from JSON file and set the parameters
with open('settings.json', 'r') as f:
    settings = json.load(f)
CamBacklight = settings['CamBacklight']
CamBrightness = settings['CamBrightness']
CamContrast = settings['CamContrast']
CamGain = settings['CamGain']
# create window with trackbars
cv2.namedWindow("Trackbar")
cv2.resizeWindow("Trackbar", 1000, 200)
cv2.createTrackbar("Backlight:", "Trackbar", CamBacklight, 2, emptyfunc)
cv2.createTrackbar("Brightness:", "Trackbar", CamBrightness, 64, emptyfunc)
cv2.createTrackbar("Contrast:", "Trackbar", CamContrast, 64, emptyfunc)
cv2.createTrackbar("Gain:", "Trackbar", CamGain, 64, emptyfunc)
while True:
    # Get trackbar positions
    CamBacklight = cv2.getTrackbarPos("Backlight:", "Trackbar")
    CamBrightness = cv2.getTrackbarPos("Brightness:", "Trackbar")
    CamContrast = cv2.getTrackbarPos("Contrast:", "Trackbar")
    CamGain = cv2.getTrackbarPos("Gain:", "Trackbar")
    # Set variables to trackbar value
    cap.set(cv2.CAP_PROP_BACKLIGHT, CamBacklight)
    cap.set(cv2.CAP_PROP_BRIGHTNESS, CamBrightness)
    cap.set(cv2.CAP_PROP_CONTRAST, CamContrast)
    cap.set(cv2.CAP_PROP_GAIN, CamGain)
    # read image from camera and show
    succes, img = cap.read()
    cv2.imshow("window name", img)
    # refresh the frames every 30ms, exit when q is pressed.
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
# Make sure the windows actually closes.
cv2.destroyAllWindows()
settings['CamBacklight'] = CamBacklight
settings['CamBrightness'] = CamBrightness
settings['CamContrast'] = CamContrast
settings['CamGain'] = CamGain
with open('settings.json', 'w') as f:
    json.dump(settings, f)
