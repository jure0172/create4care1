/* Stepper.h - Stepper library for STM32 - Version 1.0
 * 
 * Copyright (C) 2020 Pim Stoopman - All Rights Reserved
 * 
 * You may use, distribute and modify this code under the
 * terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. Either version 2.1 of the 
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * NOTE:
 */
#program once

#include <cstdlib>

// Global:
enum Direction {
    CCW,
    CW
}

enum TimerFunctions {
    set,
    reset,
    update
}

enum MotorControlVersion {
    linear,
    S_curve
}

// libary interface description
class MotorControl {
    public:
        // contructors:
        MotorControl(int _stepPin, int _dirPin);

        void init(int acceleration, int deceleration, int maxSpeed);

        // move to an absolut position:
        void moveTo_ABS(int _absPos);

        // move to a relative position:
        void moveTo_REL(int _relPos);

        // move in a direction:
        void moveTo_DIR(Direction _dir);

    private:
        void calculateBehaviour(int _stopPosition);
        void TimerControlFunction(TimerFunctions _function);

        // Test nieuwe berekeningen
        void calculateBehaviourNew(int _stopPosition);
        // movemont vars:
        struct Direction direction;
        int direction_test;
        int currentPos;
        int targetPos;
        bool stepState;
        bool motorIsMoving;

        // speed parameters:
        int acceleration;
        int deceleration;
        int maxSpeed;

        int stepPin;
        int dirPin;
        int Pulse;
        int Direction;

    protected:
        int currentRelativePosition;
        int relativeEndPosition;

        int accelerationEndPoint;
        int decellerationStartPoint;

        int time;

        int frequency_Timer;

        int A;
        int A_pre;
        int AA;
        int V;
        int J;
        int maxV;
}