/* Stepper.h - Stepper library for STM32 - Version 1.0
 * 
 * Copyright (C) 2020 Pim Stoopman - All Rights Reserved
 * 
 * You may use, distribute and modify this code under the
 * terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. Either version 2.1 of the 
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * NOTE:
 */

#include "MotorControl.h"

MotorControl::MotorControl(int _stepPin, int _dirPin)
{
    stepPin = _stepPin;
    dirPin = _dirPin;


}

void MotorControl::init()
{
    AA = A * A_pre;
}

void MotorControl::moveTo_ABS(int _absPos)
{
    if (targetPos != _absPos)
    {
        /* 
         *  
         */
    }
    else
    {
        /* don't change the move settings */
    }
    
}


/* Dit is mijn eerste oplossing om de timer te bedienen.
 * De code van de timer staat in de functie TimerHandler.
 * Deze code heb ik geplot in grafieken waar ik heb gevonden
 * dat hij geen rekening houdt met een gelijke opstart curve.
 * Een verbetere versie wordt er gemaakt in de functie "calculateBehaviourNew()"
 */
void MotorControl::calculateBehaviour(int _stopPosition)
{
    currentRelativePosition = 0;
    long accelerationPulsesToMaxSpeed = maxSpeed * maxSpeed / acceleration / 2;
    long decellerationPulsesFromMaxSpeed = maxSpeed * maxSpeed / deceleration / 2;

    if (!motorIsMoving)
    {
        relativeEndPosition = abs(targetPos - currentPos);
        Direction_test = targetPos > currentPos;

        long stepsOnFullSpeed = relativeEndPosition - accelerationPulsesToMaxSpeed - decellerationPulsesFromMaxSpeed;
        if (stepsOnFullSpeed > 0)
        {
            accelerationEndPoint = accelerationPulsesToMaxSpeed;
            decellerationStartPoint = relativeEndPosition - decellerationPulsesFromMaxSpeed;
        }
        else
        {
            accelerationEndPoint = decellerationStartPoint = relativeEndPosition * deceleration / (acceleration + deceleration);
        }
        // timer instelfunctie aanroepen
        TimerControlFunction(set);
        motorIsMoving = true;
    }
    else if (motorIsMoving)
    {
        long stepsToStop = (frequency_Timer * frequency_Timer) / (deceleration * 2.0);

        if (Direction == targetPos > currentPos && abs(targetPos - currentPos) >= stepsToStop)
        {
            currentRelativePosition = 0;
            relativeEndPosition = anbs(targetPos - currentPos);
        }
        else if (Direction == targetPos < currentPos || abs(targetPos - currentPos) < stepsToStop)
        {
            currentRelativePosition = 0;
            relativeEndPosition = stepsToStop;
        }

        long accelerationPulsesToMaxSpeedMoving = (maxSpeed * maxSpeed - frequency_Timer * frequency_Timer) / acceleration / 2;
        long pulsesOnFullSpeed = relativeEndPosition - accelerationPulsesToMaxSpeedMoving - decellerationPulsesFromMaxSpeed;
        if (pulsesOnFullSpeed > 0)
        {
            accelerationEndPoint = accelerationPulsesToMaxSpeedMoving;
            decellerationStartPoint = decellerationPulsesFromMaxSpeed;
        }
        else
        {
            long relativeSpeedLocation = (accelerationPulsesToMaxSpeed - accelerationPulsesToMaxSpeedMoving);
			accelerationEndPoint = decellerationStartPoint = ((relativeEndPosition + relativeSpeedLocation) * decelleration_Timer / (acceleration_Timer + decelleration_Timer)) - relativeSpeedLocation;
        }
        
    }
    
}

void MotorControl::TimerControlFunction(TimerFunctions _function)
{
    switch (_function)
    {
        case set:
            break;

        case reset:
            break;

        case update:
            break;
    }
}

volatile void TimerHandler(MotorControl *control)
{
	
	if (!control->Pulse)
	{
		if (control->currentRelativePosition < control->accelerationEndPoint || (control->frequency_Timer < control->maxSpeed && control->currentRelativePosition < control->decellerationStartPoint))
		{
			control->frequency_Timer += 1 / control->frequency_Timer * control->acceleration;
		}
		else if (control->currentRelativePosition > control->decellerationStartPoint || control->frequency_Timer > control->maxSpeed)
		{
			control->frequency_Timer -= 1 / control->frequency_Timer * control->decelleration;
			if (control->frequency_Timer <= 0)
			{
				control->frequency_Timer = control->frequency0_Timer;
			}
		}
		else
		{

		}
	}

	//Position administration 
	else if (control->Pulse)
	{
		if (control->Direction == CCW) // nog niet werkend !!!
			control->currentPos -= 1;

		if (control->Direction == CW) // nog niet werkend !!!
			control->currentPos += 1;

		control->currentRelativePosition++;
	}

	if (control->currentRelativePosition == control->relativeEndPosition)
	{
        // stop timer
        control->TimerControlFunction(reset);
		control->StepperMoving = false;
	}
    else
    {
        // set nieuw frequention in timer.
        control->TimerControlFunction(update);
    }
    
	// set output direction
    // set output pulse


}



void MotorControl::calculateBehaviourNew(int _stopPosition)
{
    relativeEndPosition = abs(targetPos - currentPos);

    
}